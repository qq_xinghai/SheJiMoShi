//简单工厂模式(违反开闭原则)
#include<iostream>
using namespace std;

class Product{//功能基类
	public:
		virtual void fun()=0;
		virtual ~Product(){} 
};

class Product1:public Product{
	public:
		void fun(){
			cout<<"Product1.com"<<endl;
		}
};

class Product2:public Product{
	public:
		void fun(){
			cout<<"Product2.com"<<endl;
		}
};

class Product3:public Product{
	public:
		void fun(){
			cout<<"Product3.com"<<endl;
		}
};

class Factory{
	private:
		Product * p;
	public:
		Factory(int x){
			switch(x)
			{
				case 1:p=new Product1();break;
				case 2:p=new Product2();break;
				case 3:p=new Product3();break;
			}
		}
		void Do(){
			p->fun();
		}
		~Factory(){
			delete p;
		}
};


int main(void)
{
	Factory * f=new Factory(1);
	f->Do();
	delete f;
	return 0;
} 