#include<iostream>
#include<cstring>
#include<vector>//数组模板
using namespace std;


class AbstractFile{//基类 
	protected:
		string name;
	public:
		AbstractFile(string n)
		{
			name=n;
		}
		AbstractFile(){name="XXX";}
	virtual void out()=0;
	virtual void Add(AbstractFile *){}
	virtual ~AbstractFile(){};
};

class File:public AbstractFile{//文件 
	public :
		File(string n):AbstractFile(n){}
		File(){}
		void out()
		{
			cout<<"File:"<<name<<endl;
		}
		~File(){}
};

class Folder:public AbstractFile{//文件夹 
	private: 
		vector<AbstractFile *> vec;//定义一个vec动态数组类型为AbstractFile*
	public:
		Folder(string n):AbstractFile(n){}
		Folder(){}
		void Add(AbstractFile * t)
		{
			vec.push_back(t);//将t存如动态数组中 
		}
		void out()
		{
			vector<AbstractFile *>::iterator it;//iterator自带指针类型
			cout<<name<<endl;
			for(it=vec.begin();it!=vec.end();it++)
				(*it)->out();
		}
		~Folder(){}
}; 

int main(void)
{
	Folder * root=new Folder("F:\\");
	File * file1=new File("1.c");
	File * file2=new File("2.c");
	Folder * cf=new Folder("aa");
	root->Add(file1);
	root->Add(file2);
	root->Add(cf);
	root->out();
	delete root;
	delete file1;
	delete file2;
	delete cf;
	return 0;
} 